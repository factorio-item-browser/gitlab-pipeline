# Gitlab Pipeline

This repository contains the Gitlab Pipeline files used by the other repositories of this group. Each repository does 
have its own pipeline configuration within the project directory.
